- [大数据生态圈完整知识体系_000X000的博客-CSDN博客](https://blog.csdn.net/ytp552200ytp/article/details/126364222?spm=1001.2014.3001.5502)
- [大数据生态圈完整知识体系_Impl_Sunny的博客-CSDN博客](https://blog.csdn.net/u011487470/article/details/126848706)

随着[大数据](https://so.csdn.net/so/search?q=大数据&spm=1001.2101.3001.7020)行业的发展，大数据生态圈中相关的技术也在一直迭代进步，作者有幸亲身经历了国内大数据行业从零到一的发展历程，通过本文希望能够帮助大家快速构建大数据生态圈完整知识体系。

目前大数据生态圈中的核心技术总结下来如图1所示，分为以下9类，下面分别介绍。

![img](https://img-blog.csdnimg.cn/img_convert/216c015d434a6a3a1fcd5d8052f01adc.png)

一、数据采集技术框架

数据采集也被称为数据同步。随着互联网、移动互联网、物联网等技术的兴起，产生了海量数据。这些数据散落在各个地方，我们需要将这些数据融合到一起，然后从这些海量数据中计算出一些有价值的内容。此时第一步需要做的是把数据采集过来。数据采集是大数据的基础，没有数据采集，何谈大数据！

数据采集技术框架包括以下几种：

- Flume、Logstash和FileBeat常用于日志数据实时监控采集，它们之间的细节区别见表1：

![img](https://img-blog.csdnimg.cn/img_convert/5d1674e365b25ee8cbf6b536bc9ee7dc.png)

- Sqoop和Datax常用于关系型数据库离线数据采集，它们之间的细节区别见表2：

![img](https://img-blog.csdnimg.cn/img_convert/98c895dca4590b9efe34b1ddf17ed6be.png)

- Cannal和Maxwell常用于关系型数据库实时数据采集，它们之间的细节区别见表3：

![img](https://img-blog.csdnimg.cn/img_convert/4e1d1ca90ce1a96342d788ae96a3041a.png)

Flume、Logstash和FileBeat的技术选型如图2所示：

![img](https://img-blog.csdnimg.cn/img_convert/8476b934e54cbf9db7d3daeff385c4a8.png)

Sqoop和Datax之间的技术选型如图3所示：

![img](https://img-blog.csdnimg.cn/img_convert/bc39a70e39230afd5ebc795fc193422e.png)

Cannal和Maxwell之间的技术选型如图4所示：

![img](https://img-blog.csdnimg.cn/img_convert/8026735473ad0b81daed8a6496baf084.png)

二、数据存储技术框架

数据的快速增长推动了技术的发展，涌现出了一批优秀的、支持分布式的存储系统。

数据存储技术框架包括HDFS、HBase、Kudu、Kafka等。

- HDFS它可以解决海量数据存储的问题，但是其最大的缺点是不支持单条数据的修改操作，因为它毕竟不是数据库。
- HBase是一个基于HDFS的分布式NoSQL数据库。这意味着，HBase可以利用HDFS的海量数据存储能力，并支持修改操作。但HBase并不是关系型数据库，所以它无法支持传统的SQL语法。
- Kudu是介于HDFS和HBase之间的技术组件，既支持数据修改，也支持基于SQL的数据分析功能；目前Kudu的定位比较尴尬，属于一个折中的方案，在实际工作中应用有限。
- Kafka常用于海量数据的临时缓冲存储，对外提供高吞吐量的读写能力。

三、分布式资源管理框架

在传统的IT领域中，企业的服务器资源（内存、CPU等）是有限的，也是固定的。但是，服务器的应用场景却是灵活多变的。例如，今天临时上线了一个系统，需要占用几台服务器；过了几天，需要把这个系统下线，把这几台服务器清理出来。

在大数据时代到来之前，服务器资源的变更对应的是系统的上线和下线，这些变动是有限的。随着大数据时代的到来，临时任务的需求量大增，这些任务往往需要大量的服务器资源。如果此时还依赖运维人员人工对接服务器资源的变更，显然是不现实的。因此，分布式资源管理系统应运而生，常见的包括YARN、Kubernetes和Mesos，它们的典型应用领域如图5所示。

![img](https://img-blog.csdnimg.cn/img_convert/5998d3beb90107e648b9364b7cda85d2.png)

四、数据计算技术框架

数据计算分为离线数据计算和实时数据计算。

\1. 离线数据计算

大数据中的离线数据计算引擎经过十几年的发展，到目前为止主要发生了3次大的变更。

- MapReduce可以称得上是大数据行业的第一代离线数据计算引擎，主要用于解决大规模数据集的分布式并行计算。MapReduce计算引擎的核心思想是，将计算逻辑抽象成Map和Reduce两个阶段进行处理。
- Tez计算引擎在大数据技术生态圈中的存在感较弱，实际工作中很少会单独使用Tez去开发计算程序。
- Spark最大的特点就是内存计算：任务执行阶段的中间结果全部被放在内存中，不需要读写磁盘，极大地提高了数据的计算性能。Spark提供了大量高阶函数（也可以称之为算子），可以实现各种复杂逻辑的迭代计算，非常适合应用在海量数据的快速且复杂计算需求中。

\2. 实时数据计算

业内最典型的实时数据计算场景是天猫“双十一”的数据大屏。数据大屏中展现的成交总金额、订单总量等数据指标，都是实时计算出来的。用户购买商品后，商品的金额就会被实时增加到数据大屏中的成交总金额中。

- Storm主要用于实现实时数据分布式计算。
- Flink属于新一代实时数据分布式计算引擎，其计算性能和生态圈都优于Storm。
- Spark中的SparkStreaming组件也可以提供基于秒级别的实时数据分布式计算功能。

他和Storm、Flink之间的区别见表4。

![img](https://img-blog.csdnimg.cn/img_convert/1b15c8f1f6a0c8e9206814be293ee7ae.png)

Storm、Spark、Flink 之间的技术选型如图6所示。

![img](https://img-blog.csdnimg.cn/img_convert/8b4db48793bec16f9b5502ee3f81fca4.png)

**目前企业中离线计算主要使用Spark，实时计算主要使用Flink。**

五、数据分析技术框架

数据分析技术框架包括Hive、Impala、Kylin、Clickhouse、Druid、Drois等，它们的典型应用场景如图7所示。

![img](https://img-blog.csdnimg.cn/img_convert/a70395d1392371e767487d049a2e66e5.png)

Hive、Impala和Kylin属于典型的离线OLAP数据分析引擎，主要应用在离线数据分析领域，它们之间的区别见表5。

![img](https://img-blog.csdnimg.cn/img_convert/8aaae81c602105765f101ddce2d9d545.png)

表5

- Hive的执行效率一般，但是稳定性极高；
- Impala基于内存可以提供优秀的执行效率，但是稳定性一般；
- Kylin通过预计算可以提供PB级别数据毫秒级响应。

Clickhouse、Druid和Drois属于典型的实时OLAP数据分析引擎，主要应用在实时数据分析领域，它们之间的区别见表6。

![img](https://img-blog.csdnimg.cn/img_convert/10fa6aca29b4f94af882a2a6d661ea91.png)

- Druid和Doris是可以支持高并发的，ClickHouse的并发能力有限；Druid中的SQL支持是有限的，ClickHouse支持非标准SQL，Doris支持标准SQL，对SQL支持比较好。
- 目前Druid和ClickHouse的成熟程度相对比较高，Doris处于快速发展阶段。

六、任务调度技术框架

包括Azkaban、Ooize、DolphinScheduler等。它们适用于普通定时执行的例行化任务，以及包含复杂依赖关系的多级任务进行调度，支持分布式，保证调度系统的性能和稳定性，它们之间的区别见表7。

![img](https://img-blog.csdnimg.cn/img_convert/7340e7ac751452eadd0b2be23f445e84.png)

它们之前的技术选型如图8所示。

![img](https://img-blog.csdnimg.cn/img_convert/820985904b70001e109be7c4bae08285.png)

七、大数据底层基础技术框架

大数据底层基础技术框架主要是指Zookeeper。Zookeepe主要提供常用的基础功能（例如：命名空间、配置服务等），大数据生态圈中的Hadoop（HA）、HBase、Kafka等技术组件的运行都会用到Zookeeper。

八、数据检索技术框架

随着企业中数据的逐步积累，针对海量数据的统计分析需求会变得越来越多样化：不仅要进行分析，还要实现多条件快速复杂查询。例如，电商网站中的商品搜索功能，以及各种搜索引擎中的信息检索功能，这些功能都属于多条件快速复杂查询的范畴。

在选择全文检索引擎工具时，可以从易用性、扩展性、稳定性、集群运维难度、项目集成程度、社区活跃度这几个方面进行对比。Lucene、Solr和Elasticsearch的对比见表8。

![img](https://img-blog.csdnimg.cn/img_convert/8e4f56b404661c80040b481aba9b3bf5.png)

九、大数据集群安装管理框架

企业如果想从传统的数据处理转型到大数据处理，首先要做就是搭建一个稳定可靠的大数据平台。

一个完整的大数据平台需要包含数据采集、数据存储、数据计算、数据分析、集群监控等功能，这就意味着其中需要包含Flume、Kafka、Haodop、Hive、HBase、Spark、Flink等组件，这些组件需要部署到上百台甚至上千台机器中。

如果依靠运维人员单独安装每一个组件，则工作量比较大，而且需要考虑版本之间的匹配问题及各种冲突问题，并且后期集群维护工作也会给运维人员造成很大的压力。

于是，国外一些厂商就对大数据中的组件进行了封装，提供了一体化的大数据平台，利用它可以快速安装大数据组件。目前业内最常见的是包括CDH、HDP、CDP等。

- HDP：全称是 Hortonworks Data Platform。它由 Hortonworks 公司基于 Apache Hadoop 进行了封装，借助于 Ambari 工具提供界面化安装和管理，并且集成了大数据中的常见组件， 可以提供一站式集群管理。HDP 属于开源版免费大数据平台，没有提供商业化服务；
- CDH：全称是 Cloudera Distribution Including Apache Hadoop。它由 Cloudera 公司基于 Apache Hadoop 进行了商业化，借助于 Cloudera Manager 工具提供界面化安装和管理，并且集成了大数据中的常见组件，可以提供一站式集群管理。CDH 属于商业化收费大 数据平台，默认可以试用 30 天。之后，如果想继续使用高级功能及商业化服务，则需要付费购买授权，如果只使用基础功能，则可以继续免费使用；
- CDP：Cloudera 公司在 2018 年 10 月份收购了 Hortonworks，之后推出了新一代的大数据平台产品 CDP（Cloudera Data Center）。CDP 的版本号延续了之前 CDH 的版本号。从 7.0 版本开始， CDP 支持 Private Cloud（私有云）和 Hybrid Cloud（混合云）。CDP 将 HDP 和 CDH 中比较优秀的组件进行了整合，并且增加了一些新的组件。

三者的关系如图9所示。

![img](https://img-blog.csdnimg.cn/img_convert/75df1e0643b3805c908df28edc029634.png)