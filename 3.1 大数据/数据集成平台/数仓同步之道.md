- [【数据同步】数仓同步之道_Impl_Sunny的博客-CSDN博客](https://blog.csdn.net/u011487470/article/details/120737224)

# 一、Sqoop

Sqoop，SQL-to-[Hadoop](https://so.csdn.net/so/search?q=Hadoop&spm=1001.2101.3001.7020) 即 “SQL到Hadoop和Hadoop到SQL”。

是[Apache](https://so.csdn.net/so/search?q=Apache&spm=1001.2101.3001.7020)开源的一款在Hadoop和关系数据库服务器之间传输数据的工具。主要用于在Hadoop与关系型数据库之间进行数据转移，可以将一个关系型数据库（MySQL ,Oracle等）中的数据导入到Hadoop的HDFS中，也可以将HDFS的数据导出到关系型数据库中。

sqoop命令的本质是转化为[MapReduce](https://so.csdn.net/so/search?q=MapReduce&spm=1001.2101.3001.7020)程序。sqoop分为导入（import）和导出（export），策略分为table和query，模式分为增量和全量。

![img](https://img-blog.csdnimg.cn/047ce0c8cf2c4adbb50383cb47125dad.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBASW1wbF9TdW5ueQ==,size_20,color_FFFFFF,t_70,g_se,x_16)

 命令简单示例：

![img](https://img-blog.csdnimg.cn/3500914e457a4f4bb02f948746559ef9.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBASW1wbF9TdW5ueQ==,size_20,color_FFFFFF,t_70,g_se,x_16)

# 二、DataX

DataX 是阿里巴巴集团内被广泛使用的离线数据同步工具/平台，实现包括 MySQL、[Oracle](https://so.csdn.net/so/search?q=Oracle&spm=1001.2101.3001.7020)、SqlServer、Postgre、HDFS、Hive、ADS、HBase、TableStore(OTS)、MaxCompute(ODPS)、DRDS 等各种异构数据源之间高效的数据同步功能。

github地址：https://github.com/alibaba/DataX

支持数据源：

![img](https://img-blog.csdnimg.cn/838ac6fb94a4458687ff9167d130022e.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBASW1wbF9TdW5ueQ==,size_20,color_FFFFFF,t_70,g_se,x_16)

DataX本身作为离线数据同步框架，采用Framework + plugin架构构建。将数据源读取和写入抽象成为Reader+Writer插件，纳入到整个同步框架中。

目前已到datax3.0框架设计：

![img](https://img-blog.csdnimg.cn/8c5309b0eeb74406955f61b0abf1a57a.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBASW1wbF9TdW5ueQ==,size_20,color_FFFFFF,t_70,g_se,x_16)

 datax使用示例，核心就是编写json配置文件job：

![img](https://img-blog.csdnimg.cn/8ec4e85d77294a2abfb18251f587b224.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBASW1wbF9TdW5ueQ==,size_20,color_FFFFFF,t_70,g_se,x_16) 

# 三、Kettle

Kettle，中文名：水壶，是一款国外免费开源的、可视化的、功能强大的ETL工具，纯java编写，可以在Windows、Linux、Unix上运行，数据抽取高效稳定。

Kettle家族目前包括4个产品：Spoon、Pan、CHEF、Kitchen。

Kettle的最大特点：

- 免费开源：基于Java免费开源软件
- 易配置：可跨平台，绿色无需安装
- 不同数据库：ETL工具集，可管理不同数据库的数据
- 两种脚本文件：transformation和job，transformation完成针对数据的基础转换，job则完成整个工作流的控制
- 图形界面设计：托拉拽，无需写代码
- 定时功能：在Job下的start模块，有一个定时功能，可以每日，每周等方式进行定时

# 四、Canal

canal是阿里巴巴旗下的一款开源项目，纯Java开发。基于数据库增量日志解析，提供增量数据实时订阅和消费，目前主要支持了MySQL，也支持mariaDB。

很多大型的互联网项目生产环境中使用，包括阿里、美团等都有广泛的应用，是一个非常成熟的数据库同步方案，基础的使用只需要进行简单的配置即可。

github地址：https://github.com/alibaba/canal

当前的 canal 支持源端 MySQL 版本包括 5.1.x , 5.5.x , 5.6.x , 5.7.x , 8.0.x

![img](https://img-blog.csdnimg.cn/62c0ceb312e64eb3b062a5809f33f1a5.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBASW1wbF9TdW5ueQ==,size_20,color_FFFFFF,t_70,g_se,x_16)

 canal是通过模拟成为mysql 的slave的方式，监听mysql 的binlog日志来获取数据，binlog设置为row模式以后，不仅能获取到执行的每一个增删改的脚本，同时还能获取到修改前和修改后的数据，基于这个特性，canal就能高性能的获取到mysql数据数据的变更。

# 五、**StreamSets**



Streamsets是一个大数据实时采集ETL工具，可以实现不写一行代码完成数据的采集和流转。通过拖拽式的可视化界面，实现数据管道(Pipelines)的设计和定时任务调度。

数据源支持MySQL、Oracle等结构化和半/非结构化，目标源支持[HDFS](https://so.csdn.net/so/search?q=HDFS&spm=1001.2101.3001.7020)、Hive、Hbase、Kudu、Solr、Elasticserach等。创建一个Pipelines管道需要配置数据源(Origins)、操作(Processors)、目的地(Destinations)三部分。

Streamsets的强大之处：

- 拖拽式可视化界面操作，No coding required 可实现不写一行代码
- 强大整合力，100+ Ready-to-Use Origins and Destinations，支持100+数据源和目标源
- 可视化内置调度监控，实时观测数据流和数据质量

![img](https://img-blog.csdnimg.cn/4b943b2c1fc549c096f091555de92efd.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBASW1wbF9TdW5ueQ==,size_20,color_FFFFFF,t_70,g_se,x_16)

 